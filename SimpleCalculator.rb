#! /Users/admin/.rvm/rubies/ruby-3.0.0/bin/ruby
class SimpleCalculator
    ALLOWED_OPERATIONS = ['+', '/', '*' ,'-'].freeze
    def self.calculate( arg1, arg2, operation)
      raise ArgumentError if arg1.class == String || arg2.class == String
      raise UnsupportedOperation unless ALLOWED_OPERATIONS.include? operation
      begin
      "#{arg1} #{operation} #{arg2} = #{arg1.send(operation, arg2)}"
      rescue ZeroDivisionError => e
        "Division by zero is not allowed."
      end
    end
    class UnsupportedOperation < StandardError
    end
  end
  
  puts SimpleCalculator.calculate(16, 51, "+")
  puts SimpleCalculator.calculate(32, 6, "*")
  puts SimpleCalculator.calculate(512, 4, "/")
  
  puts SimpleCalculator.calculate(1, '2', '*')
  puts SimpleCalculator.calculate(1, 2, '-')
  puts SimpleCalculator.calculate(512, 0, "/")
  
  
  