#! /Users/admin/.rvm/rubies/ruby-3.0.0/bin/ruby

class LogLineParser
    def initialize(line)
      @line = line
    end
    def message
      @line.partition(":").last.strip
    end
    def log_level
      @line[/(?<=\[).*?(?=\])/].downcase
    end
    def reformat
      "#{message} (#{log_level})"
    end
  end
  
      puts LogLineParser.new('[ERROR]: Invalid operation').message
    puts LogLineParser.new("[WARNING]:  Disk almost full\r\n").message
    puts LogLineParser.new('[ERROR]: Invalid operation').log_level
  puts LogLineParser.new('[INFO]: Operation completed').reformat