#! /Users/admin/.rvm/rubies/ruby-3.0.0/bin/ruby

class BirdCount
    def self.last_week
      return [0, 2, 5, 3, 7, 8, 4]
      #raise 'Please implement the BirdCount.last_week method'
    end
  
    def initialize(birds_per_day)
      @birds_per_day=birds_per_day
      #raise 'Please implement the BirdCount#initialize method'
    end
  
    def yesterday
      @birds_per_day[-2]
      #raise 'Please implement the BirdCount#yesterday method'
    end
  
    def total
      @birds_per_day.sum
      #raise 'Please implement the BirdCount#total method'
    end
  
    def busy_days
      @birds_per_day.count{|number| number >= 5}
      #raise 'Please implement the BirdCount#busy_days method'
    end
  
    def day_without_birds?
      @birds_per_day.any?{|number| number == 0}
      #raise 'Please implement the BirdCount#day_without_birds method'
    end
  end

  
  puts BirdCount.last_week
  puts "--------"
  birds_per_day = [2, 5, 0, 7, 4, 1]
  bird_count = BirdCount.new(birds_per_day)
  puts bird_count.yesterday
  puts "--------"
  puts bird_count.total
  puts "--------"
  puts bird_count.busy_days
  puts "--------"
  puts bird_count.day_without_birds?


