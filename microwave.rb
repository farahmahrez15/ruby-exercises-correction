#! /Users/admin/.rvm/rubies/ruby-3.0.0/bin/ruby

class Microwave
    attr_reader :timer
    def initialize(s)
      i = "%04d"%s
    sec = i[2..3].to_i % 60
    min = i[2..3].to_i/60 + i[0..1].to_i
    @timer = "%02d:%02d"%[min, sec]
    end
  end