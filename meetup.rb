#! /Users/admin/.rvm/rubies/ruby-3.0.0/bin/ruby

require "date"
class Meetup
  def initialize(month, year)
    @month = month
    @year = year
  end
  def day(weekday, schedule)
    range = (Date.new(@year, @month, 1)..Date.new(@year, @month, -1))
    selection = range.select { |date| date.send("#{weekday}?") }
    case schedule
    when :first
      selection.first
    when :second
      selection[1]
    when :third
      selection[2]
    when :fourth
      selection[3]
   when :last

      selection.last
    when :teenth
      selection.find { |date| date.day.between?(13, 19) }
    end
  end

end
